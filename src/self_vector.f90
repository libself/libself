!>
!! Provides array-based, self-extending reference vectors of arbitrary type.
!!
module self_vector

    use self_vector_ref

    implicit none

    private

    type, extends(vector_ref), public :: vector
    contains
        procedure :: destroy ! TODO: use 'final', but compiler support is limitted atm
        procedure :: add_item
        procedure :: init
        procedure :: init_from_array
    end type

contains

    !
    ! type(vector)
    !

    ! constructors

    subroutine init(this, verbose, initial_size)
        logical, optional, intent(in) :: verbose
        integer, optional, intent(in) :: initial_size
        class(vector), intent(out) :: this

        call this%vector_ref%init(verbose, initial_size)
    end subroutine init

    subroutine init_from_array(this, items, verbose, initial_size)
        class(*), target, intent(in) :: items(:)
        logical, optional, intent(in) :: verbose
        integer, optional, intent(in) :: initial_size
        class(vector), intent(out) :: this

        call this%vector_ref%init_from_array(items, verbose, initial_size)
    end subroutine init_from_array

    recursive subroutine destroy(this)
        class(vector),intent(inout) :: this
        type(vector_iterator) :: i
        class(*), pointer :: my_stuff

        call this%get_iter(i)
        do while(i%next(my_stuff))
            if(associated(my_stuff)) then
                deallocate(my_stuff)
            end if
        end do

    end subroutine destroy

    recursive subroutine add_item(this, new_stuff)
        class(vector),intent(inout) :: this
        class(*), target, intent(in) :: new_stuff
        class(*), pointer :: my_stuff
        if(this%get_verbose()) print *, 'vector%add_item begin'
        allocate(my_stuff, source=new_stuff)
        call this%vector_ref%add(my_stuff)
        if(this%get_verbose()) then
            print *, 'added to vector (', this%length(), '/', &
                     this%capacity(), ' elements)'
        end if
        if(this%get_verbose()) print *, 'vector%add_item end'
    end subroutine add_item

end module self_vector

