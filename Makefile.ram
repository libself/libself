TESTS   = 
#ESTS  += test/test_vector
FFLAGS  =
MOD_DIR_OPTIONS =
DEBUG   = false
FC      = gfortran

VPATH   = src

# {{{ some flags for intel, nag and gcc
GCCFFLAGS   := -cpp -march=native -O2 -mpc64 -g -fbacktrace -fbounds-check -fstack-protector-all -D__LOOP_EXCHANGE -finit-real=nan -finit-integer=-2147483648 -finit-character=127 -Wall -Wcharacter-truncation -Wconversion -Wunderflow -Wunused-parameter -fmodule-private -fimplicit-none -fmax-identifier-length=63 -ffree-line-length-132 -frecursive
INTELFFLAGS := -check bounds -check pointers -check uninit -debug -O2 -msse2 -fltconsistency -g -pc64 -fpp -traceback -standard-semantics
ICON_INTEL_FFLAGS := -ftz -O2 -march=native -pc64 -fp-model source -g
ICON_INTEL_FFLAGS_SELF := -ftz -O2 -march=native -pc64 -fp-model source -g -standard-semantics
NAGFFLAGS   := -gline -g -colour -fpp -Wc,-O0 -Wc,-march=native -float-store -nan -f2008
# }}}

# choose compiler flags
ifeq ($(FC),nagfor)
  FFLAGS += $(NAGFFLAGS)
endif
ifeq ($(FC),ifort)
  #FFLAGS += $(INTELFFLAGS)
  FFLAGS += $(ICON_INTEL_FFLAGS)
  #FFLAGS += $(ICON_INTEL_FFLAGS_SELF)
  FFLAGS += -standard-semantics
endif
ifeq ($(FC),gfortran)
  FFLAGS += $(GCCFFLAGS)
endif
# setup the options for specifing place where to pu the mod files {{{
# NOT USED
ifeq ($(FC),nagfor)
  MOD_DIR_OPTIONS = 
endif
ifeq ($(FC),gfortran)
  MOD_DIR_OPTIONS = -J
endif
ifeq ($(FC),ifort)
  MOD_DIR_OPTIONS = -module
endif
# }}}
#
ifeq ($(DEBUG),true)
  ifeq (nagfor,$(FC))
     FFLAGS += -g -w=all -gline -colour
    #FFLAGS += -g -gline -colour
  else
    #FFLAGS += -fbacktrace -g -Wall 
    FFLAGS += -fbacktrace -g -fcheck=all -frecursive
  endif
endif

ifneq ($(FC),nagfor)
FFLAGS += $(MOD_DIR_OPTIONS) ./mods
endif

all: libself.a(self_object.o self_vector_ref.o self_vector.o self_map_ref.o self_map.o self_assert.o)


-include ./Makefile.autotests
-include Makefile.depend


.PHONY: doc depend Makefile.autotests

clean:
	$(RM) $(TARGETS) *.o *.mod src/*.a src/*.mod src/*.o test/*_actual.txt test/src/*.mod test/src/*.o test/lib/*.mod test/lib/*.o $(TESTS)
	rake -f $(VPATH)/../Rakefile clean


test/test_vector:              src/libself.a
test/test_map_on_outputNml:    src/libself.a
# load external test targets
Makefile.autotests:
	@ rake DEBUG=$(DEBUG) -f $(VPATH)/../Rakefile VPATH=$(VPATH)

Makefile.depend:
	@ rake -f $(VPATH)/../Rakefile VPATH=$(VPATH) depend > Makefile.depend

depend: Makefile.depend

check: all $(TESTS)
	@ for test in $(TESTS); do \
	    echo '#=============================================================' ; \
	    if [ "$$test" = 'test/test_vector' ] ; then \
	      printf "$$test: " ; \
	      $(RM) test/test_vector_actual.txt ; \
	      test/test_vector | sed 's/  */ /g' > test/test_vector_actual.txt ; \
	      diff test/test_vector_actual.txt test/test_vector_expected.txt > /dev/null 2>&1 && echo ok || { echo FAIL; exit 1; } ; \
	    else \
	      echo "$$test: " ; \
	     ./$$test ; \
	    fi ; \
	  done ;
	@ echo '#============================================================='

checkAllCompilers:
	@source /sw/share/Modules/init/bash
	@module purge
	echo "Check with NAG 6.0.1038"
	@module switch nag nag/6.0.1038
	@make clean all check FC=nagfor
	@module purge
	echo "Check with NAG 5.3.951"
	@module switch nag nag/5.3.951
	@make clean all check FC=nagfor
	@module purge
	echo "Check with INTEL 14.0.2"
	@module switch intel intel/14.0.2
	@make clean all check FC=ifort
	@module purge
	echo "Check with INTEL 15.0.2"
	@module switch intel intel/15.0.2
	@make clean all check FC=ifort
	@module purge
	echo "Check with GCC 5.1.0"
	@module switch gcc gcc/5.1.0
	@make clean all check FC=gfortran

checkInput:
	@echo "CC     = $(CC)"
	@echo "FC     = $(FC)"
	@echo "FFLAGS = $(FFLAGS)"
	@echo "DEBUG  = $(DEBUG)"
	@echo "COMPILE.f  = $(COMPILE.f)"
	@echo "COMPILE.F  = $(COMPILE.F)"

doc:
	@doxygen

help:
	@echo "Following targets are available"
	@echo "all [default]:    - build the self library with given fortran compiler FC"
	@echo "clean             - rm temporary files"
	@echo "check             - run all available tests: test/test*f90 test/auto/*f90"
	@echo "doc               - create documentation with doxygen"
	@echo "checkAllCompilers - check library with multiple compilers on thunder"
	@echo "checkInput        - print some variables"

%: %.f90
	pwd
	$(LINK.f) $^ $(LOADLIBES) $(LDLIBS) -Itest/lib -Isrc -o $@

%.o: %.f90
	pwd
	$(COMPILE.f) -o $@ $< 

%: %.F90
	pwd
	$(LINK.F) $^ $(LOADLIBES) $(LDLIBS) -Itest/lib -Isrc -o $@

%.o: %.F90
	pwd
	$(COMPILE.F) -o $@ $< 

%.f90: %.F90
	pwd
	$(PREPROCESS.F) $(OUTPUT_OPTION) $<
