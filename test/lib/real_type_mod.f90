module real_type_mod

    use self_object

    implicit none

    public

    type, extends(object) :: real_type
        real :: x
    contains
        procedure :: is_equal
        procedure :: to_string
    end type

contains

    logical function is_equal(this, that)
        class(real_type), intent(in) :: this
        class(*), intent(in) :: that
        select type(that)
            class is (real_type)
                is_equal = this%x == that%x
            class default
                is_equal = .false.
        end select
    end function is_equal

    function to_string(this)
        class(real_type), intent(in) :: this
        character(:), allocatable :: to_string
        to_string = 'real_type('//object_string(this%x)//')'
    end function to_string

end module
