!! author: Karl-Hermann Wieners (karl-hermann.wieners@mpimet.mpg.de)
program test_vector

    use self_vector 

    implicit none

    type :: number_type
        integer :: value
    end type

    type (vector) :: main_vector
    type (vector) :: slave_vector
    class(*), pointer :: my_value_A, my_value_B

    integer :: nr

    integer, target :: numbers(10) = (/(nr, nr = 1, 10)/)
    character(6), target :: names(10) = (/'eins  ', 'zwei  ', 'drei  ', 'vier  ', &
        'fuenf ', 'sechs ', 'sieben', 'acht  ', 'neun  ', 'zehn  '/)
    type(number_type), target :: inumbers(10) = (/(number_type(nr), nr = 1, 10)/)
    integer :: position

    main_vector = vector(numbers, debug=.true., initial_size=5)
    slave_vector = vector(debug=.true.)

    do nr=1,10
        call main_vector%add( names(nr) )
    end do

    do nr=1,10
        call main_vector%add( inumbers(nr) )
    end do
!   !TODO error, multiple adds of 10 instead of 0..10 {{{
!   do nr=1,10
!       print *,'ADD:',real(nr)
!       call main_vector%add( real(nr) )
!   end do
!   ! }}}

    ! check get/set {{{
    position = main_vector%length() - 5
    my_value_A => main_vector%get_item(position)
 
    call main_vector%set(position,3456)
    my_value_B => main_vector%get_item(position)
 
    ! set above limit
    call main_vector%set(position+1,1234)
    ! get above limit
    my_value_B => main_vector%get_item(position+1)
    print *,'my_value_B at position ',position+1,': '
    call class_print(my_value_B)
    print *,', expected 1234'
    ! }}}

    call print_vector('main', main_vector)
!   call main_vector%print('internal print')

    call main_vector%clone(slave_vector)
    call print_vector('slave', slave_vector)
    if (main_vector%length() .ne. slave_vector%length()) then
      print *,'ERROR: clone methods creates different vectors'
    end if

    call main_vector%clone(slave_vector)
    call main_vector%clear
    call print_vector('slave (reprise)', slave_vector)

    ! Have to call this because final does not work!
    call slave_vector%clear

contains

    recursive function next_integer(this, item) result(valid)
        class(vector_iterator), intent(inout) :: this
        logical :: valid
        integer, intent(out) :: item
        class(*), pointer :: buffer
        valid = this%next(buffer)
        if(valid) then
            select type(buffer)
            type is (integer)
                item = buffer
            class default
                valid = next_integer(this, item)
            end select
        end if
    end function next_integer

    subroutine justprint(buffer)
        class(*), intent(in) :: buffer
        select type(buffer)
        type is (character(*))
            print *, "'", trim(buffer), "'"
        type is (number_type)
            print *, 'number_type(', buffer%value, ')'
        type is (integer)
            print *, 'integer:', buffer
        type is (real)
            print *, 'real:', buffer
        end select
    end subroutine
    subroutine print_vector(label, my_vector) 
        character(*), intent(in) :: label
        class(vector), target, intent(in) :: my_vector
        type(vector_iterator) :: my_iter
        class(*), pointer :: buffer
        integer :: i, ibuffer

        print *, label
        print *,'Vector length:',my_vector%length()

        print *, 'typed iter loop'
        my_iter = my_vector%each()
        !!! my_iter = vector_iterator(my_vector)
        do while(next_integer(my_iter, ibuffer))
            print *, ibuffer
        end do

        print *, 'do loop'
        do i = 1, my_vector%length()
            buffer => my_vector%get_item(i)
            call justprint(buffer)
        end do

        print *, 'iter loop'
        my_iter = my_vector%each()
        !!! my_iter = vector_iterator(my_vector)
        do while(my_iter%next(buffer))
            !!! buffer = my_iter%next()
            call justprint(buffer)
        end do
    end subroutine print_vector

end program test_vector

! vim:fdm=marker tw=0
