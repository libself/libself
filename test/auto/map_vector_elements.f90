type(map) :: m
type(vector) :: v,w
class(*),pointer :: vv,ww,vvv
character(len=256) :: var_names(4), output_names(4)
character(len=256) :: key_names(4)

call m%init
call v%init
call w%init

var_names(1)    = 'temp'
var_names(2)    = 'u'
var_names(3)    = 'v'
output_names(1) = 't'
output_names(2) = 'u'
output_names(3) = 'v'
key_names(1) = 'PT01H'
key_names(2) = 'PT06H'
key_names(3) = 'PT12H'

call v%add_list(var_names(1:3));    if (DEBUG) print *, v%to_string()
call w%add_list(output_names(1:3)); if (DEBUG) print *, w%to_string()

call m%add(key_names(1),v)
call m%add(key_names(2),w)

vv => m%get(key_names(1))
ww => m%get(key_names(2))

call assert_equal(v,vv)
call assert_equal(w,ww)
call assert_not_equal(v,w)
call assert_not_equal(vv,ww)

call v%add(output_names(1))
call assert_not_equal(v,vv)

! not add it back to the map
call m%add(key_names(1),v)
vvv => m%get(key_names(1))
call assert_equal(v,vvv)
call assert_not_equal(w,vvv)

if (DEBUG) call print_summary('complete map {{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{')
if (DEBUG) print m%to_string()
if (DEBUG) call print_summary('}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}')
