    type(vector) :: v_preset_real, v_preset_logical, v_preset_string
    type(vector) :: v_preset_int, string_vector, mixed_vector, copy_vector
    integer      :: i, init(4),inits(5)
    real         :: r(5),rr
    double precision :: d
    logical      :: l(3)
    character(2) :: s(4),ss(2)

    string_vector = vector()
    call string_vector%add('P01D')
    call string_vector%add('P10D')
    call string_vector%add('PT01H')
    call string_vector%add('PT12H')

    call assert(string_vector%includes('P10D'))!, message='Could not find "P10D"')
    call assert(.not. string_vector%includes('asdasdasd'))!, message='Found unexpected  "asdasdasd"')

    init           = (/1,2,3,4/)
    v_preset_int   = vector(init,debug=DEBUG)

    call assert_equal(4,v_preset_int%length())
    call assert(v_preset_int%includes(3))
    call assert(.not. v_preset_int%includes(33))

    l = (/.true.,.false.,.false./)
    v_preset_logical = vector(l)
    call assert_equal(3,v_preset_logical%length())
    call assert(v_preset_logical%includes(.true.))

    r = (/1.1, 2.2, 3.3, 3.141529,0.0/)
    v_preset_real = vector(r)
    d = 1.123456789d3
    call v_preset_real%add(d)
    call assert_equal(6,v_preset_real%length(),' Vector has length idfferent to 6')
    call assert(v_preset_real%includes(1.1),'1.1 not found!')
    call assert(v_preset_real%includes(2.2),'2.2 not found!')
    call assert(v_preset_real%includes(3.3),'3.3 not found!')
    call assert(v_preset_real%includes(3.141529),'3.141529 not found!')

    call v_preset_real%clone(copy_vector)

    if (DEBUG) call v_preset_real%print('BEFORE DOUBLE CHECK')
    rr = 3.14152985205
    call assert(.not.  v_preset_real%includes(rr))
    if (DEBUG) print *,'---------------------------------------------------------------------'
    d = 1.123456789d3
    call assert(v_preset_real%includes(d))
    if (DEBUG) print *,'---------------------------------------------------------------------'
    d = 1123456789d-1
    call assert(      v_preset_real%includes(d))
    if (DEBUG) print *,'---------------------------------------------------------------------'
    if (DEBUG) call v_preset_real%print('AFTER DOUBLE CHECK')
!
!   mixed_vector = vector()
!   call assert_equal(0,mixed_vector%length())
