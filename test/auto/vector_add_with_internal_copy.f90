type(vector_ref)      :: v
type(vector)      :: w
integer           :: i
real              :: r
double precision  :: d
character(len=10) :: s
logical           :: l
class(*), pointer           :: ii, rr, dd, ss, ll

!{{{ put plain variable wihtou copy - later change should lead to changed vector elements
call v%init(verbose=DEBUG)
i = 11
r = 22.2
d = 33.3d0
s = 'fortyfor'
l = .false.

call v%add(i)
call v%add(r)
call v%add(d)
call v%add(s)
call v%add(l)

ii => v%at(1)
rr => v%at(2)
dd => v%at(3)
ss => v%at(4)
ll => v%at(5)

call assert_equal(11,ii)
call assert_equal(22.2,rr)
call assert_equal(33.3d0,dd)
call assert_equal('fortyfor',ss)
call assert_equal(.false.,ll)

l = .true.
s = '0123456789'
i=123
r = 222.2
d = 333.3d0
call assert_equal(222.2,rr)
call assert_equal(333.3d0,dd)
call assert_equal(i,ii)
call assert_equal('0123456789',ss)
call assert_equal(.true.,ll)


!}}}
!
!!{{{ now WITH copy
call w%init
i = 11
r = 22.2
d = 33.3d0
s = 'fortyfor'
l = .false.

nullify(ii)
nullify(rr)
nullify(dd)
nullify(ss)
nullify(ll)

call w%add(i)
call w%add(r)
call w%add(d)
call w%add(s)
call w%add(l)
!
ii => w%at(1)
rr => w%at(2)
dd => w%at(3)
ss => w%at(4)
ll => w%at(5)

call assert_equal(11,ii)
call assert_equal(22.2,rr)
call assert_equal(33.3d0,dd)
call assert_equal('fortyfor',ss)
call assert_equal(.false.,ll)
! reset initial values and pointers and check again
l = .true.
s = '0123456789'
i=123
r = 222.2
d = 333.3d0
nullify(ii)
nullify(rr)
nullify(dd)
nullify(ss)
nullify(ll)
ii => w%at(1)
rr => w%at(2)
dd => w%at(3)
ss => w%at(4)
ll => w%at(5)
call assert_equal(11,ii)
call assert_equal(22.2,rr)
call assert_equal(33.3d0,dd)
call assert_equal('fortyfor',ss)
call assert_equal(.false.,ll)
!!}}}
