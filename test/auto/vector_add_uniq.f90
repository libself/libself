
  type(vector) :: vecA, vecB, vecSomething
  character(1) :: keysA(4) = (/'A','B','C','D'/)
  integer      :: keysB(4) = (/1,2,3,4/)
  character(1) :: values(4) = (/'A','B','C','D'/)
  integer      :: i,j,k
  real         :: r, rr, s, ss(2)

  vecA = vector()
  vecB = vector()
  
  do i=1,4
    call vecA%add(values(i))
    call vecB%add(values(i))
  end do

  ! check if elements can be found
  call assert(vecA%includes('A'),'Cannot find "A" in vecA')
  ! check if non-existing elements cannot be found
  call assert( .not. vecA%includes('X'),'Could find "X" in vecA')
  call assert_equal(4,vecA%length(),'unexpected length')

  ! check uniq adding
  call vecA%addUniq('X')
  call assert(vecA%includes('X'),'Cound not find "X"')
  call assert_equal(5,vecA%length(),'unexpected length')
  call vecA%addUniq('A')
  do i=1,4 ; call vecA%addUniq(values(i)) ; end do
  call assert_equal(5,vecA%length(),'unexpected length')

  ! check return index
  call assert_equal(3 ,vecB%get_index('C'),             ' unexpected index:  3')
  call assert_equal(3 ,vecB%addUniqAndReturnIndex('C'), ' unexpected index:  3')
  call assert_equal(1 ,vecB%get_index('A'),             ' unexpected index:  1')
  call assert_equal(1 ,vecB%addUniqAndReturnIndex('A'), ' unexpected index:  1')
  call assert_equal(-1,vecB%get_index('X'),             ' unexpected index: -1')
  call assert_equal(5 ,vecB%addUniqAndReturnIndex('X'), ' unexpected index:  5')

  ! check adding and uniq adding of vectors'
  call vecA%clear(); call vecB%clear()
  call assert_equal(0,vecA%length(),'unexpected nonzero length')
  call assert_equal(0,vecB%length(),'unexpected nonzero length')
  do i=1,4
    call vecA%add(values(i))
  end do

  call assert_equal(4,vecA%length(),'unexpected length')
  ! Add vecA to vecB ...'
  call vecB%add(vecA)
  call assert_equal(1,vecB%length(),'unexpected length')
  ! AddUniq vecA to vecB ...'
  call vecB%addUniq(vecA)
  call assert_equal(1,vecB%length(),'unexpected length')
  call vecB%get(1,vecSomething)
  call assert_equal(vecA,vecSomething,' added vector is not equal to the original one')

  ! check with real numbers
  call vecA%clear()
   s     = 1.2345
  call vecA%add(s)!.2345)
  call vecA%add(12.345)
  call vecA%addUniq(12.345)
  call assert_equal(2,vecA%length())
  call assert(vecA%includes(1.2345))
  call assert(vecA%includes(12.345))
  call vecA%get(2,r)
  call assert_equal(12.345,r)
  call vecA%get(1,rr)
  if (DEBUG) print *,rr
  call assert_equal(rr,s)
  call vecA%add(.true.)
  call assert(vecA%includes(.true.))
