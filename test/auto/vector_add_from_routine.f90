type(map) :: m
type(vector) :: v,w
type(vector) :: vv,ww
type(vector) :: vvv,www
character(len=256) :: var_names(4), output_names(4)
character(len=256) :: key_names(4)

type t_fields 
  character(len=16) :: name
  real, pointer     :: r_ptr
end type
type(t_fields) :: fields

m               = map()
v               = vector()
w               = vector()

var_names(1)    = 'temp'
var_names(2)    = 'u'
var_names(3)    = 'v'
output_names(1) = 't'
output_names(2) = 'u'
output_names(3) = 'v'
key_names(1)    = 'PT01H'
key_names(2)    = 'PT06H'
key_names(3)    = 'PT12H'

call localAdd(v,w)
call v%add(key_names(1))
call v%add(key_names(2))
call v%add(key_names(3))
key_names(3)    = 'XXXXX'
if (DEBUG) call print_blue('Vector WITHOUT COPY <-------------------------')
if (DEBUG) call v%print()
if (DEBUG) call print_blue('Vector WITH    COPY <-------------------------')
if (DEBUG) call w%print()

contains
  subroutine localAdd(v,w)
    type(vector) :: v,w

    real , save, target :: r
    integer , save, target :: i,j
    character(len=256) , save, target :: str

    DO i=1,3

      call random_number(r)
      j = floor(r*1000)
      if (DEBUG) write(str,'(F8.4)') r

      call v%add(r)
      call v%add(trim(str))
      call v%add(j)
      call v%add(111*i)

      call w%add(r,copy=.true.)
      call w%add(trim(str),copy=.true.)
      call w%add(j,copy=.true.)

      call w%add(111*i,copy=.true.)

      if (DEBUG) print *,'r=',r
      if (DEBUG) print *,'s=',trim(str)
      if (DEBUG) print *,'j=',j
      if (DEBUG) print *,'----------------------------'
    end do

  end subroutine
