
    !
    ! Object equality
    !

    ! integer
    call assert_not_equal(1, 2, verbose=DEBUG)
    call assert_equal(1, 1, verbose=DEBUG)

    ! string
    call assert_not_equal('eins', 'zwei', verbose=DEBUG)
    call assert_equal('eins', 'eins', verbose=DEBUG)

    ! real_type
    call assert_not_equal(real_type(1.0), real_type(2.0), verbose=DEBUG)
    call assert_equal(real_type(1.0), real_type(1.0), verbose=DEBUG)

    !
    ! String conversion
    !

    ! integer
    call assert_equal(object_string(1), '1', verbose=DEBUG)

    ! string
    call assert_equal(object_string('eins'), "'eins'", verbose=DEBUG)

    ! real_type
    call assert_equal(object_string(real_type(1.0)), &
        'real_type('//object_string(1.0)//')', verbose=DEBUG)
