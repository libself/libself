    type(map)           :: hash
    type(vector)        :: vec,mykeys,myvals
    integer, target     :: a
    character(1),target :: c
    integer             :: values(4) = (/1,2,3,4/)
    character(1)        :: keys(4)   = (/'A','B','C','D'/)
    integer             :: i

    call vec%init
    call hash%init(verbose=DEBUG,initial_size=10)

    a = 0
    c = '0'

    call hash%set_verbose(.false.)
    call assert( .not. hash%get_verbose())
    call hash%add(c,a)
    call assert_equal(1, hash%length())
    call hash%set_verbose(.true.)
    call assert(hash%get_verbose())
    call hash%set_verbose(DEBUG)


    do i=1,4 ; call vec%add(values(i)); call vec%add(keys(i)); end do

    do i=1,4 ; call hash%add(keys(i),values(i)); end do
    call assert_equal(5, hash%length(),'unexpected length, expected 5')
    do i=1,4 ; call hash%add(keys(i),values(i)); end do
    call assert_equal(5, hash%length(),'unexpected length, expected 5')

    call hash%add('vec',vec)
    call assert_equal(6, hash%length(),'unexpected length, expected 5')

    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call assert_equal(6, hash%length(),'unexpected length, expected 6')


    mykeys = hash%keys();   if (DEBUG) print *,mykeys%to_string()
    myvals = hash%values(); if (DEBUG) print *,myvals%to_string()

    !COMPARE: vec vs. map

    if (DEBUG) call print_summary('==========================================================')
    if (DEBUG) print *, vec%to_string()
    if (DEBUG) call print_summary('==========================================================')
    if (DEBUG) print *, hash%to_string()
    if (DEBUG) call print_summary('==========================================================')
    call assert_not_equal(vec,hash,'unexpected vec == hash')
    call assert_not_equal(hash,vec,'unexpected hash == vec')
    !TODO: implement class_equal for map class
    !TODO: should be EQUAL
    call assert(hash%is_equal(hash),'unexpected hash != hash')
    call assert(mykeys%is_equal(hash%keys()),'expected unequal map keys')
    if (DEBUG) print *,'call mykeys%set(1,"foo")'
    call mykeys%set(1,'foo')
    call assert_not_equal(hash%keys(),mykeys)

    call mykeys%clear()
    call assert_equal(0,mykeys%length())
    mykeys = hash%keys();
    if (DEBUG) print *, mykeys%to_string()

    call assert_equal(6, hash%length(),'unexpected length, expected 6')
    call hash%clear()
    call assert_equal(0, hash%length(),'unexpected length, expected 0')
    if (DEBUG) print *,'=========================================================='
    if (DEBUG) print *,'CHECK map%has_key'
    call assert( .not. hash%has_key('vec'),' unexpected: T')
    call hash%add('vec',vec)
    call assert( hash%has_key('vec'),' unexpected: F')
    call assert( .not. hash%has_key('foo'),' unexpected: T')
    if (DEBUG) print *,'=========================================================='
    if (DEBUG) print *,'CHECK map%has_value'
    call assert( .not. hash%has_value('vec'),' unexpected: T')
    call assert(       hash%has_value( vec ),' unexpected: F')
    call assert( .not. hash%has_value('foo'),' unexpected: T')
    call hash%add(vec,'foo')
    call assert(       hash%has_value('foo'),' unexpected: F')
    if (DEBUG) print *,'=========================================================='
    if (DEBUG) print *,'CHECK get/get_by_index'
    call hash%clear
    call vec%clear

    call hash%clear()
    call assert_equal(0,hash%length())
    call hash%add('vec',vec)
    call assert_equal(1,hash%length())
