    !
    ! Basic tests for vector class
    !

    integer :: i

    type(real_type), target :: test_data(10) = [ (real_type(i*i), i = 1, 10) ]

    type(vector) :: vector1, vector2, vector3
    
    !call test_types
    !call test_add_list
    call test_add_vector

contains

    subroutine test_types()
        type(vector) :: vec, inner
        type(vector_iterator) :: iter
        class(*), pointer :: stuff
        
        inner = vector()
        call inner%add(6)
        call inner%add(6)
        call inner%add(6)
        print *, inner%to_string()

        ! check implicit allocation based on inner
        allocate(stuff, source=inner)
        print*,object_pointer_string(stuff)

        vec = vector(verbose=DEBUG)
        call vec%add(.true.)
        call vec%add(2)
        call vec%add(3.0)
        call vec%add('four')
        call vec%add(real_type(5.0))
!       call vec%add(stuff)
        iter = vec%iter()
        do while(iter%next(stuff))
            print *, iter%pos(), object_pointer_string(stuff)
        end do
        !!! print *, vec%to_string()
    end subroutine

    subroutine test_add_list()
        vector1 = vector(verbose=DEBUG)
        vector2 = vector(verbose=DEBUG)

        ! Fill vector with single adds
        do i = 1, 10
            print *, "add", i
            call vector1%add(test_data(i))
            print *, object_string(vector1)
        end do

        ! Compare single adds with array add
        call vector2%clear
        call vector2%add_list(test_data)
        call assert_equal(vector1, vector2, "add_list: add_list(array)")

        ! Compare single adds with vector add
        call vector2%clear
        call vector2%add_list(vector1)
        call assert_equal(vector1, vector2, "add_list: add_list(vector)")

        ! Compare single adds with iterator add
        call vector2%clear
        call vector2%add_list(vector1%iter())
        call assert_equal(vector1, vector2, "add_list: add_list(iterator)")

    end subroutine test_add_list

    subroutine test_add_vector()
        type(vector) :: va,vb
        type(vector_iterator) :: i
        class(*), pointer :: x

        vA = vector()
        vB = vector()

        call vA%add(57)
        call vA%add(3.4)
        call vA%add('68')

        call vB%add(vA)

        i = vB%iter()
        do while(i%next(x))
            print *,i%pos(),object_pointer_string(x)
        enddo
    end subroutine test_add_vector
