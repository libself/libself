    type :: list_element
      integer :: key
      class(*), pointer :: next
    end type


    type (vector) :: main_vector
    type (vector) :: copy_vector ,copy_vector_assign
    type (vector) :: assigned_vector,v

    integer :: nr, assigned_int, i,j,k,l

    integer, target :: numbers(10) = (/(nr, nr = 1, 10)/)
    character(6), target :: &
      & names(10) = (/'eins  ', 'zwei  ', 'drei  ', 'vier  ', 'fuenf ', 'sechs ', 'sieben', 'acht  ', 'neun  ', 'zehn  '/)
    class(*), pointer :: my_value_A, my_value_B
    integer           :: position, length_A, length_B!, length_C

    type(list_element), target  :: elemA, elemB
    class(*),           pointer :: getA,  getB

    character(10)   :: s

    elemA%key=0; elemA%next => elemB
    elemB%key=1; elemB%next => elemA

    main_vector = vector(numbers, debug=DEBUG, initial_size=500)
    do nr=1,10
        call main_vector%add( numbers(nr) )
    end do
    do nr=1,10
        call main_vector%add( names(nr) )
    end do

    ! check get/set {{{
    position = main_vector%length() - 2
    call assert_equal(28,position)
    my_value_A => main_vector%get_item(position)
    call assert_equal('acht',my_value_A)
 
    call main_vector%set(position,3456)
    my_value_B => main_vector%get_item(position)
    call assert_equal(3456,my_value_B)
    ! }}}
 
    ! set above limit {{{
    call  main_vector%clone(copy_vector)
    copy_vector_assign = main_vector
    call assert_equal(copy_vector_assign,copy_vector)

    length_A = main_vector%length()
    call main_vector%set(position+1000,1234) ! should not work
    length_B = main_vector%length()
    call assert_equal(length_A, length_B)

    call main_vector%set(main_vector%length()+1,1234) ! should not work
    ! length should be idential
    call assert_equal(length_A, length_B)

    call main_vector%add(1234) ! should not work
    ! length should be idential
    call assert_equal(length_A, length_B)
    ! }}}

    ! add complex element
    call main_vector%add(elemA)
    call main_vector%add(elemB)
 
    getA => main_vector%get_item(main_vector%length()-1)
    getB => main_vector%get_item(main_vector%length()  )
 
!   call assert_equal(elemA,getA,    'unexpected elemA != getA')
!   call assert_not_equal(elemA,getB,'unexpected elemA == getB')
!   call assert_not_equal(elemB,getA,'unexpected elemB == getA')
 
    ! check for getting items from a vector of the right class, i.e. NOT of
    ! class(*)
    call main_vector%get(1,assigned_int)
    call assert_equal(1,assigned_int)

    v = vector()
    call v%add('0')
    i = v%get_index('0')
    call assert_equal(i,1)
    call v%get(i,s)
    call assert_equal('0',s)

    l = 0
    call v%add(l)
    k = v%get_index(0)
    call assert_equal(k,2)
    call v%get(k,l)
    call assert_equal(0,l)
