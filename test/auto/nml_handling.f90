!TODO toberemoved
  integer, parameter :: N = 5
  integer, parameter :: number_of_periods = 4
  integer            :: i,j
  character(5) :: periods(number_of_periods)   = (/'PT06H','P01Y ','P01M ',' P01D'/)
  character(5) :: period
  character(100) :: meanH(N) = '',meanD(N)='', meanY(N)='',meanM(N)='',varname
  character(100) :: minH(N),minD(N), minY(N),minM(N)
  character(100) :: maxH(N),maxD(N), maxY(N),maxM(N)
  character(100) :: divH(N),divD(N), divY(N),divM(N)
  type(map)     :: meanMap, meanMapManual
  type(vector)  :: varVector,v
  type(vector)  :: meanHVector, meanHVector_add_list
  type(vector)  :: meanDVector
  type(vector)  :: meanMVector
  type(vector)  :: meanYVector
!
! !     "PT06H"=> {
! !        "mean"=>["temp", "tend_t_up", "u", "v", "zmc"],
    meanH(1) = "temp"; meanH(2) = "tend_t_up"; meanH(3) = "u"; meanH(4) = "v";meanH(5) = "zmc"
! !        "min"=>["tend_t_up", "u", "v", "zmc"],
! !        "max"=>["tend_t_up", "u", "v", "zmc"],
! !        "div"=>["temp", "tend_t_up", "u", "v", "zmc"]},
! !!     "P01D"=> {
! !        "mean"=>["temp", "tend_t_up", "zmc"],
    meanD(1) = "temp"; meanD(2) = "tend_t_up"; meanD(3) = "zmc"
!   meanDVector = vector(meanD)
! !        "min"=>["tend_t_up", "zmc"],
! !        "max"=>["tend_t_up", "zmc"],
! !        "div"=>["temp", "tend_t_up", "zmc"]},
! !!     "P01M"=> {
! !        "mean"=>["temp", "tend_t_up", "u", "v", "zmc"],
    meanM(1) = "temp"; meanM(2) = "tend_t_up"; meanM(3) = "u"; meanM(4) = "v";meanM(5) = "zmc"
!   meanMVector = vector(meanM)
! !        "min"=>["tend_t_up", "u", "v", "zmc"],
! !        "max"=>["tend_t_up", "u", "v", "zmc"],
! !        "div"=>["temp", "tend_t_up", "u", "v", "zmc"]},
! !!     "P01Y"=> {
! !        "mean"=>["temp", "u", "v", "zmc"],
    meanY(1) = "temp"; meanY(2) = "u"; meanY(3) = "v";meanY(4) = "zmc"
!   meanYVector = vector(meanY)
! !        "min"=>["u", "v", "zmc"],
! !        "max"=>["u", "v", "zmc"],
! !        "div"=>["temp", "u", "v", "zmc"]}}
!
    meanHVector          = vector()
    meanHVector_add_list = vector()
    do i=1,5; call meanHVector%add(meanH(i)); end do
    call meanHVector_add_list%add_list(meanH)
    call assert_equal(5,meanHVector%length(),'unexpected length != 5')
    call assert_equal(meanHVector, meanHVector_add_list)

    meanDVector = vector()
    call meanDVector%add_list(meanD)

    meanMVector = vector()
    call meanMVector%add_list(meanM)

    meanYVector = vector()
    call meanYVector%add_list(meanY)

   ! (1) construct a map from variable lists
   meanMap      = map(debug=DEBUG,initial_size=10)
   meanMapManual= map(debug=DEBUG,initial_size=10)
   v = vector()

   call v%add_list(periods)
   if (DEBUG)  call v%print('PERIODS')
   if (DEBUG)  print *,periods
   if (DEBUG)  print *,'periods(1):',trim(periods(1))
   if (DEBUG)  print *,( trim(periods(2)) .EQ. trim(periods(3)) )

   call meanMapManual%add(trim(periods(1)),meanHVector_add_list)
   if (DEBUG)  print *,'periods(2):',trim(periods(2))
   call meanMapManual%add(trim(periods(2)),meanDVector)
   if (DEBUG)  print *,'periods(3):',trim(periods(3))
   call meanMapManual%add(trim(periods(3)),meanMVector)
   if (DEBUG)  print *,'periods(4):',trim(periods(4))
   call meanMapManual%add(trim(periods(4)),meanYVector)

   do i=1,number_of_periods
     period = periods(i)
     if ( meanMap%has_key(period) ) then
       call meanMap%get(period,varVector)
     else
       if (DEBUG) call print_aqua('Createa new Vector')
       varVector = vector(debug=.false.)
     end if
     do j=1,N
       select case (j)
       case (1)
         varname = meanH(j)
         call varVector%addUniq(trim(varname))
       case (2)
         varname = meanD(j)
         call varVector%addUniq(trim(varname))
       case (3)
         varname = meanM(j)
         call varVector%addUniq(trim(varname))
       case (4)
         varname = meanY(j)
         call varVector%addUniq(trim(varname))
       end select
     end do
     call meanMap%add(trim(period),varVector)
   end do

   if (DEBUG)  call print_aqua(' MANUAL ==================================================')
   if (DEBUG)  call meanMapManual%print()
   if (DEBUG)  call print_aqua(' AUTOMATIC ===============================================')
   if (DEBUG)  call meanMap%print()
   if (DEBUG)  call print_aqua(' =========================================================')
