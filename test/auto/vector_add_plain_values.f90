type(vector) :: v,w
integer :: i,j
real    :: r,s
logical :: l,k
character(len=5) :: c,d

v = vector()
w = vector()

call addSomePlainValues(v)
call addSomePlainValues(w)

call v%get(1,c)
call v%get(2,d)
call v%get(3,i)
call v%get(4,j)
call v%get(5,r)
call v%get(6,s)
call v%get(7,l)
call v%get(8,k)


!call assert_equal()
call assert_equal('ABCDE', d)
call assert_equal(22222,   j)
call assert_equal(11.2233, s)
call assert_equal(.false., k)

if (DEBUG) call print_blue("SHOW ALL ITEMS: {{{{{{{")
if (DEBUG) call v%print()
if (DEBUG) call print_blue("}}}}}}}}}}}}}}}}}}}}}}}")

contains
  subroutine addSomePlainValues(vec)
    type(vector) :: vec

    call vec%add('abcde')
    call vec%add('ABCDE', copy=.true.)

    call vec%add(11111)
    call vec%add(22222,copy=.true.)

    call vec%add(1.23)
    call vec%add(11.2233,copy=.true.)

    call vec%add(.true.)
    call vec%add(.false.,copy=.true.)
  end subroutine
