    type(map)           :: hash_zero, hash_ten, hash, h1, h2
    type(vector)        :: vec,mykeys,myvals
    integer, target     :: a
    character(1),target :: c
    integer             :: values(4) = (/1,2,3,4/) , i
    character(1)        :: keys(4)   = (/'A','B','C','D'/)

    call vec      %init()
    call hash     %init(verbose=DEBUG,initial_size=10)
    call hash_ten %init(verbose=DEBUG,initial_size=10)
    call hash_zero%init()

    ! compare two empty maps w/o different initial_size
    call assert(hash%is_equal(hash_ten))
    call assert_equal(hash%is_equal(hash_ten), hash_ten%is_equal(hash))
    call assert_equal(hash%is_equal(hash_zero), hash_zero%is_equal(hash))

    a = 0
    c = '0'

    call hash%set_verbose(.false.)
    call assert( .not. hash%get_verbose())
    call hash%add(c,a)
    call assert( .not. hash%get_verbose())
    if (DEBUG) print *,hash%to_string()
    call assert_equal(1, hash%length(),' unexpected length != 1')

    ! compore maps, which are supposed to be different
    call hash_ten%add(a,c)
    call hash_zero%add(c,a)
    call assert( .not. hash_ten%is_equal(hash_zero))
    call hash_zero%add(a,c)
    call hash_ten%add(c,a)
    ! maps differ se of different internal ordering
    call assert( .not. hash_ten%is_equal(hash_zero))

    call hash_ten%clone(h1)
    call h1%clone(h2)
    call assert(hash_ten%is_equal(h1))
    call assert(hash_ten%is_equal(h2))
    call assert(h1%is_equal(h2))
    call assert(0 < h1%length())
    call assert(0 < h2%length())
    call assert_equal(h1%keys(),h2%keys())
    call assert_equal(h1%values(),h2%values())

    do i=1,4 ; call vec%add(values(i)); call vec%add(keys(i)); end do
    if (DEBUG) print *,'=========================================================='
    if (DEBUG) print *,'vec:'
    if (DEBUG) print *,vec%to_string()

    if (DEBUG) print *,' ADD INTEGERS ============================================'
    do i=1,4 ; call hash%add(keys(i),values(i)); end do
    call assert_equal(5,hash%length())

    if (DEBUG) print *,'Check uniqness of entries'
    do i=1,4 ; call hash%add(keys(i),values(i)); end do
    call assert_equal(5,hash%length())

    call hash%add('vec',vec)
    call assert_equal(6,hash%length())
    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call hash%add('vec',vec)
    call assert_equal(6,hash%length())

    if (DEBUG) print *,'=========================================================='
    mykeys = hash%keys();   if (DEBUG) print *,mykeys%to_string()
    myvals = hash%values(); if (DEBUG) print *,myvals%to_string()

    if (DEBUG) print *,'=========================================================='
    if (DEBUG) print *,'COMPARE: vec vs. map'
    call assert( .not. vec%is_equal(hash),' unexpected vec == hash')
    call assert( .not. hash%is_equal(vec),' unexpected hash == vec')
!   call assert(       class_equal(hash,hash),', unexpected hash != hash')
    call assert_equal(hash%keys(),mykeys ,' unexpected keys != keys()')

    ! keys should return a real copy
    call mykeys%set(1,'foo')
    call assert_not_equal(hash%keys(),mykeys,' unexpected hash%keys == mykeys')

    if (DEBUG) print *,'=========================================================='
    if (DEBUG) print *,'CHECK map%clear'
    call hash%clear()
    call assert_equal(0, hash%length(),' unexpected non-zero length')

    if (DEBUG) print *,'=========================================================='
    if (DEBUG) print *,'CHECK map%has_key'
    call assert( .not. hash%has_key("vec"),' unexpected key "vec" found')
    call hash%add('vec',vec)
    call assert(       hash%has_key("vec"),' unexpected key "vec" missing')
    call assert( .not. hash%has_key("foo"),' unexpected key "foo" found')

    if (DEBUG) print *,'=========================================================='
    if (DEBUG) print *,'CHECK map%has_value'
    call assert( .not. hash%has_value('vec'),' unexpected value "vec" found')
    call assert(       hash%has_value( vec ),' unexpected value vec missing')
    call assert( .not. hash%has_value('foo'),' unexpected value "foo" found')
