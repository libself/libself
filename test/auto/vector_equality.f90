  type(vector) :: vecA, vecB, vecAB, vecMixed, getVec, vecMixedAgain
  type(vector),pointer :: vecAc, vecBc, vecABc, vecABcc
  type(vector)         :: allis, numVec, numVecCopy
  character(1) :: keysA(4) = (/'A','B','C','D'/)
  integer      :: keysB(4) = (/1,2,3,4/)
  character(1) :: values(4) = (/'A','B','C','D'/)
  integer      :: i,j,k
  real         :: r1, r2, r(5)
  double precision :: d1, d2

  vecA  = vector()
  vecB  = vector()
  vecAB = vector()

  do i=1,4
    call vecA%add(values(i))
    call vecB%add(values(i))
  end do

  if (DEBUG) print *,'=== A ===================================================='
  if (DEBUG) print *, vecA%to_string()
  if (DEBUG) print *,'=== B ===================================================='
  if (DEBUG) print *, vecA%to_string()
  if (DEBUG) print *,'=== COMPARE =============================================='
  call assert_equal(vecA,vecB)
  if (DEBUG) print *,'=== put A and B into vecAB ==============================='
  call assert_equal(0,vecAB%length(),'unexpected non-zero length')
  call vecAB%add(vecA)
  call vecAB%add(vecB)
  call assert_equal(2,vecAB%length(),'unexpected length != 2')
  call assert_equal(vecAB%at(1),vecAB%at(2))

  do i=1,4
    call vecA%add(keysA(i))
    call vecB%add(keysA(i))
  end do
  call assert_equal(8,vecA%length(),'unexpected length != 8')
  call assert_equal(8,vecB%length(),'unexpected length != 8')

  if (DEBUG) print *,'=== COMPARE =============================================='
  call assert_equal(vecA,vecB)
  if (DEBUG) print *,'=== put A and B into vecAB ==============================='
  call assert_equal(2,vecAB%length(),'unexpected length - expected:2')
  call vecAB%add(vecA)
  call vecAB%add(vecB)
  call assert_equal(4,vecAB%length(),'unexpected length - expected:2')
  call assert_equal(vecAB%at(1),vecAB%at(2))
  call assert( .not. vecA%includes('X'),' vecA should not include "X"')

! call vecA%addUniq('X')
! call assert(       vecA%includes('X'),' vecA should include "X"')
! if (DEBUG) print *,vecA%to_string()
! call assert_equal(9,vecA%length(),'unexpected length - expected 9')
! call vecA%addUniq('A')
! if (DEBUG) print *, vecA%to_string()
! call assert_equal(9,vecA%length(),'unexpected length - expected 9')
! do i=1,4 ; call vecA%addUniq(values(i)) ; end do
! call assert_equal(9,vecA%length(),'unexpected length - expected 9')
  ! check return index
  if (DEBUG) print *,'=========================================================='
  if (DEBUG) print *,'Check get_index and addUniq for given vector: ------------'
  if (DEBUG) print *, vecB%to_string()
  if (DEBUG) print *,'----------------------------------------------------------'
  if (DEBUG) print *,'index of "C" with get_index()      :',vecB%find('C'), ' expected:  3'
#if defined (WITH_SEARCH_INDICES)
  allis = vector()
  i = vecB%find('C',indices=allis)
  call assert_equal(7,i)
  call assert_equal(2,allis%length())
  call allis%get(1,i )
  if (DEBUG) print *,'i=',i
  call assert_equal(3,i)
  call allis%get(2,i )
  if (DEBUG) print *,'i=',i
  call assert_equal(7,i)
  if (DEBUG) call print_aqua('SHOW INDICES: ||||||||||||||||||')
  if (DEBUG) print *, allis%to_string()
  if (DEBUG) call print_aqua('||||||||||||||||||||||||||||||||')
#endif
  call assert_equal(3,vecB%find('C'))
! call assert_equal(7,vecB%addUniqAndReturnIndex('C'), ' unexpected index !=  7')
  call assert_equal(1,vecB%find('A'))
! call assert_equal(5,vecB%addUniqAndReturnIndex('A'), ' expected:  5')
  call assert_equal(0,vecB%find('X'))
! call assert_equal(9,vecB%addUniqAndReturnIndex('X'))

  if (DEBUG) print *,' CHECK REALS and DOUBLES ================================'
  numVec = vector()
  r = (/1.1, 2.2, 3.3, 6.0,0.0/)
  do i=1,5; call numVec%add(r(i)); end do
  d1 = 1.123456789d3
  call numVec%add(d1)
  call assert_equal(6,numVec%length())
  call numVec%clone(numVecCopy)
  call assert_equal(6,numVecCopy%length())
  if (DEBUG) print *,'NUMVEC    :', numVec%to_string()
  if (DEBUG) print *,'NUMVECCOPY:', numVecCopy%to_string()

  call assert(numVec%includes(1.1),'1.1 not found!')
  call assert(numVec%includes(2.2),'2.2 not found!')
  call assert(numVec%includes(3.3),'3.3 not found!')
  call assert(numVec%includes(6.0),'6.0 not found!')

  call assert_equal(numVec, numVecCopy)
  call assert_equal(numVec, numVecCopy)
  if (DEBUG) print *,'NUMVEC    :', numVec%to_string()
  if (DEBUG) print *,'NUMVECCOPY:', numVecCopy%to_string()
  ! prove that call inf 'includes' does not change the vector
  call assert(  .not.    numVec%includes(1111))
  call assert(  .not.    numVec%includes(9.9d1))
  call assert(           numVec%includes(d1))

! ! change one vector and check equality again
! call numVec%set(2,.true.)
! call assert_not_equal(numVec, numVecCopy)
! if (DEBUG) print *,numVec%to_string('NUMVEC:')
! if (DEBUG) print *,numVecCopy%to_string('NUMVECCOPY:')
! call numVec%set(2,2.2)
! call assert_equal(numVec, numVecCopy)

  ! not add a vector to another vector
  vecMixed = vector()
  call vecMixed%add(numVec)
  call vecMixed%add(vecA)
  call vecMixed%add(vecB)
! call vecMixed%addUniq(vecA)
  ! create another "complex" vector and compare both
  call assert_equal(vecMixed%at(vecMixed%length()), vecB)
  vecMixedAgain = vector()
  call vecMixedAgain%add(numVec)
  call vecMixedAgain%add(vecA)
  call vecMixedAgain%add(vecB)

  call assert_equal(vecMixedAgain,vecMixed)
