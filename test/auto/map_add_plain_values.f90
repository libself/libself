! TESTED: map%map map%add map%to_string map%keys vector%vector vector%add



type(map) :: m
type(vector) :: v
integer :: i

real, pointer :: r(:)

call m%init
call v%init
allocate(r(4))

r(1)=1.2
r(2)=3.4
r(3)=5.6
r(4)=7.8

do i=1,4 
  call m%add(r(i),.true.)
enddo
call addPlain(m)
call addVariables(m)

call v%add(r(1))
call v%add(r(2))
call v%add(r(3))
call v%add(r(4))
call v%add('keyP')
call v%add('keyV')

call assert_equal(v,m%keys())

call addPlain(m)
call addVariables(m)

call addPlain(m)
call addVariables(m)


if (DEBUG) print '(A)', m%to_string()

contains
  subroutine addPlain(hash)
    type(map) :: hash

    call hash%add('keyP','valP')
  end subroutine

  subroutine addVariables(hash)
    type(map) :: hash

    character(len=4) :: a,b
    a= 'keyV';b='valV'

    call hash%add(a,b)
  end subroutine
