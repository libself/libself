    type(vector)     :: v_empty,v_preset_real, v_preset_logical, v_preset_string
    type(vector)     :: v_preset_int_a, v_preset_int_b, string_vector
    integer          :: i, init(4),inits(5),is(1)
    real             :: r(5),f
    double precision :: d
    logical          :: l(3),ll
    character(2)     :: s(4),ss(2),sss
    character(5)     :: ssss
    class(*), pointer :: ip

    ! check list constructor {{{
    if (DEBUG) print *,"# INTEGERS ============================================================="
    init           = (/1,2,3,4/)
    v_preset_int_a = vector(init,verbose=DEBUG)

    inits          = (/42,1,2,3,4/)
    v_preset_int_b = vector(inits)

    i = -31

    call assert_equal(4,v_preset_int_a%length(),'Expected length 4 violated')
    call assert_equal(5,v_preset_int_b%length(),'Expected length 5 violated')

    ip => v_preset_int_a%at(1); call assert_equal(1,ip,'first element is NOT 1')
    ip => v_preset_int_a%at(4); call assert_equal(4,ip,'forth element is NOT 4')
    ip => v_preset_int_b%at(1); call assert_equal(42,ip,'first element is NOT 42')
    ip => v_preset_int_b%at(4); call assert_equal(3,ip,'forth element is NOT 5')
    ip => v_preset_int_b%at(3); call assert_equal(2,ip,'third element is NOT 3')

    call assert_equal(v_preset_int_a, v_preset_int_a)
    call assert_equal(v_preset_int_b, v_preset_int_b)
    !}}}

#if 0
    ! check default constructor {{{
    v_empty        = vector()
    call assert_equal(0,v_empty%length(),'length of empty vector non-zero')

    call v_empty%add(0)
    call assert(v_empty%includes(0))
    call v_empty%clear()
    call assert(.not. v_empty%includes(0))
    is(1) = 1

    call assert( .not. v_empty%includes(is(1)))
    call v_empty%clear()
    call v_empty%add(1)
    call assert(       v_empty%includes(is(1)))
    if (DEBUG) call v_empty%print()
    call assert_equal(v_empty, v_empty)

    if (DEBUG) print *,"# LOGICALS ============================================================="
    l = (/.true.,.false.,.false./)
    v_preset_logical = vector(l)
    call assert_equal(3,v_preset_logical%length(), 'Expected length 3 violated')

    call v_preset_logical%get(1,ll)
    call assert_equal(.true., ll)

    call v_preset_logical%get(2,ll)
    call assert_equal(.false.,ll)

    call v_preset_logical%get(3,ll)
    call assert_equal(.false.,ll)
    call assert_equal(v_preset_logical, v_preset_logical)

    if (DEBUG) print *,"# REALS ================================================================"
    r = (/1.1, 2.2, 3.3, 3.141529852754,0.0/)
    v_preset_real = vector(r)
    f = -1.0
    call v_preset_real%get(3,f); call assert_equal(3.3,f,optional_error_text='3 element is NOT 3.3',optional_force_show=DEBUG)
    call v_preset_real%get(5,f); call assert_equal(0.0,f,'last element is NOT 0.0')

    if (DEBUG) print *,"# DOUBLE PRECISION ====================================================="
    call assert_equal(v_preset_real,v_preset_real)

    if (DEBUG) print *,"# STRINGS =============================================================="
    s = (/' a','b ','cd','  '/)
    if (DEBUG) print *,s
#if defined (NAGFOR)
    v_preset_string = vector(s)
    if (DEBUG) call v_preset_string%print()
    sss = '##'
    call v_preset_string%get(3,sss)
    call assert_equal('cd',sss,optional_error_text='3 element is NOT 3.3',optional_force_show=DEBUG)
    call v_preset_string%get(4,sss)
    call assert_equal('  ',sss,'last element is NOT 0.0')
    call v_preset_string%get(4,sss)
    call assert_equal('',  sss,'last element is NOT 0.0')
#else
    !TODO: initialization with array if strings does not work with gfortran
    ! ss = (/'PT01M','PT01H'/)
    ! print *,ss
    !TODO: gfortran workaround
    v_preset_string = vector()
    ! call v_preset_string%print()
    call v_preset_string%add_list(s)
    call assert_equal(4,v_preset_string%length())
    if (DEBUG) call v_preset_string%print('NOT NAGFOR:---------------------------------------------')
#endif

    string_vector = vector()
    call string_vector%add('P01D')
    call string_vector%add('P10D')
    call string_vector%add('PT01H')
    call string_vector%add('PT12H')
    if (DEBUG) call string_vector%print()
    call string_vector%get(2, ssss); call assert_equal('P10D', ssss,'Could not find string "P10D"')
    call string_vector%get(4, ssss); call assert_equal('PT12H',ssss,'Could not find string "PT12H"')
    call assert_equal(string_vector, string_vector)

!   string_array = (//)
#endif
