    !
    ! Basic tests for map class
    !

    integer :: i

    integer, parameter :: test_size = 10
    character(len=4) :: test_keys(5)
    type(real_type) :: test_data(test_size) = [ (real_type(i*i), i = 1, test_size) ]

    type(map)    :: map1, mapToplevel
    type(vector) :: v

    test_keys = [ 'eins', 'zwei', 'drei', 'vier', 'five' ]

    ! top level additions {{{
    call mapToplevel%init()
    print *,size(test_keys)
    do i=1,size(test_keys)
      call mapToplevel%add(test_keys(i),i)
    end do
    call assert_equal(size(test_keys),5)
    call assert_equal(size(test_keys),mapToplevel%length())

    call v%init()
    do i=1,size(test_keys)
      call v%add(test_keys(i))
    end do
    call print_summary(v%to_string())
    call assert_equal(v%length(), mapToplevel%length())
!   call assert_equal(v,mapToplevel%keys()) ! fails with nag, works with gfortran
    call print_summary(mapToplevel%to_string())
    ! }}}

    ! changes within routines {{{
    call test_add
    ! }}}
    call test_select_from_keyValue
    call test_select_via_iterator
#ifndef __INTEL_COMPILER
    call test_select_from_map
#endif
    call test_iterator_with_multiple_select

contains

    subroutine test_add()

        type(vector) :: testKeys
        type(vector) :: w,wCopy
        type(map)    :: mapLocal

        call print_routine('test_add','started')
        ! add global variables
        call map1%init(verbose=DEBUG)
        do i = 1, 10
            call map1%add(i,test_data(i))
            call map1%add(i,test_data(i))
        end do
        print *,map1%to_string()

        testKeys = map1%keys()
        !allocate(testKeys,source = map1%keys())
        call print_summary('KEYS {{{{{{{{{{{{{{{{{{{{{{{{{{{',stderr=.true.)
        call print_verbose(testKeys%to_string())
        call print_summary(' }}}}}}}}}}}}}}}}}}}}}}}}}}}',stderr=.true.)
        call print_verbose(map1%to_string())

        ! add a local vector
        call w%init
        call w%add_list(testKeys)
        call print_summary('LOCAL VECTOR {{{{{{{{{{{{{{{{{{{',stderr=.true.)
        call print_verbose(w%to_string())
        call print_summary(' }}}}}}}}}}}}}}}}}}}}}}}}}}}',stderr=.true.)

        call assert_equal(test_size,map1%length())
        call assert_equal(test_size,testKeys%length())

        call print_summary('KEYS {{{{{{{{{{{{{{{{{{{{{{{{{{{')
        print *,testKeys%to_string()

        call testKeys%add('sechs')
        print *,testKeys%to_string()
        call print_summary('}}}}}}}}}}}}}}}}}}}}}}}}}}}')

        call w%clone(wCopy)
        call print_summary(wCopy%to_string())
        call print_summary(w%to_string())

        call assert_equal(test_size+1,testKeys%length(),verbose=DEBUG)
        call map1%add('keys',testKeys)
        call map1%add('values',w)
!       print *,w%to_string()
        print *,map1%to_string()
!       stop
        call assert_equal(test_size+2, map1%length())

        call mapLocal%init
        call mapLocal%add('keys',testKeys)
        call mapLocal%add('valuess',w)
        call print_summary(mapLocal%to_string())
        call print_summary(w%to_string())
        call print_summary(wCopy%to_string())

        call print_routine('test_add','finished')
    end subroutine test_add

    subroutine test_select_from_keyValue

      type(vector) :: values, keys

      call print_routine('test_select_from_keyValue','started')

      call print_summary(mapToplevel%to_string())

      values = mapToplevel%values()
      keys   = mapToplevel%keys()

      do i=1,keys%length()
      select type (something => keys%at(i))
      type is (character(*))
        call print_summary(something)
        select type (what => values%at(i))
        type is (real_type)
          call print_error(what%to_string())
        end select
      end select
      end do

      call print_routine('test_select_from_keyValue','finished')
    end subroutine test_select_from_keyValue

    subroutine test_select_from_map

      type(vector) :: keys

      call print_routine('test_select_from_map','started')

      call print_summary(mapToplevel%to_string())

      keys = mapToplevel%keys()

      do i=1,keys%length()
        select type (key => keys%at(i))
        type is (character(*))
          ! following line throws a runtime error with intel 14-16
          select type (val => mapToplevel%get(key))
          type is (real_type)
            call print_summary(object_string(val))
          end select
        end select
      end do

      call print_routine('test_select_from_map','finished')
    end subroutine test_select_from_map

    subroutine test_select_via_iterator

      type(vector_iterator) :: each
      class(*), pointer     :: stuff,k,v

      call print_routine('test_select_via_iterator','started')

      each = mapToplevel%iter()
      do while(each%next(stuff))
        print *, 'POSITION:', each%pos()
        call print_summary(  'ITEM :'//object_pointer_string(stuff))
        select type (stuff)
        type is (map_item)
          k => stuff%key
          v => stuff%value
          call print_summary('KEY          :'//object_string(stuff%key))
          call print_summary('KEY   (local):'//object_pointer_string(k))
          call print_summary('VALUE        :'//object_string(stuff%value))
          call print_summary('VALUE (local):'//object_pointer_string(v))
        end select
      end do

      call print_routine('test_select_via_iterator','finished')
    end subroutine test_select_via_iterator

    subroutine test_iterator_with_multiple_select
      type(map) :: myMap
      type(vector) :: myVector,myOtherVector

      call print_routine('test_iterator_with_multiple_select','started')

      call myMap%init
      call myVector%init
      call myOtherVector%init

      call myVector%add_list(test_keys)

      call print_error(myVector%to_string())
!     call assert_equal("['eins','zwei','drei','vier','five']",myVector%to_string())
      call myMap%add('strings',myVector)
      call assert_equal(1,myMap%length())

      call myOtherVector%add(__FILE__)
      call myOtherVector%add(__LINE__)
      call print_error(myOtherVector%to_string())

      call print_routine('test_iterator_with_multiple_select','finished')
    end subroutine test_iterator_with_multiple_select
