    !
    ! Basic tests for vector_ref class
    !

    integer :: i

    type(real_type), target :: test_data(10) = [ (real_type(i*i), i = 1, 10) ]

    type(vector_ref) :: vector1, vector2
    
    call test_types
    call test_add_list

contains

    subroutine test_types()
        type(vector_ref) :: vec, inner
        type(vector_iterator) :: iter
        class(*), pointer :: stuff
        call inner%init
        call inner%add(6)
        call inner%add(6)
        call inner%add(6)
        print *, inner%to_string()
        call vec%init(verbose=DEBUG)
        call vec%add(.true.)
        call vec%add(2)
        call vec%add(3.0)
        call vec%add('four')
        call vec%add(real_type(5.0))
        call vec%add(inner)
        iter = vec%iter()
        do while(iter%next(stuff))
            print *, iter%pos(), object_pointer_string(stuff)
        end do
        !!! print *, vec%to_string()
    end subroutine

    subroutine test_add_list()
        call vector1%init(verbose=DEBUG)
        call vector2%init(verbose=DEBUG)

        ! Fill vector with single adds
        do i = 1, 10
            call vector1%add(test_data(i))
        end do

        ! Compare single adds with array add
        call vector2%clear
        call vector2%add_list(test_data)
        call assert_equal(vector1, vector2, "add_list: add_list(array)")

        ! Compare single adds with vector add
        call vector2%clear
        call vector2%add_list(vector1)
        call assert_equal(vector1, vector2, "add_list: add_list(vector)")

        ! Compare single adds with iterator add
        call vector2%clear
        call vector2%add_list(vector1%iter())
        call assert_equal(vector1, vector2, "add_list: add_list(iterator)")

    end subroutine test_add_list
