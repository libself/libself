type(vector) :: v
class(*),pointer :: w
type(map)    :: h,g
integer      :: ints(3),i
character(1) :: s(3)

ints = (/1,2,3/)
s = (/'a','x','4'/)


call v%init_from_array(ints,verbose=DEBUG)

call h%init; call g%init

do i=1,3
  call h%add(s(i),ints(i))
  call g%add(s(i),ints(i))
  call g%add(ints(i),s(i))
end do

call assert_equal(3,h%length())
call assert_equal(6,g%length())
! add vector to a map
call h%add('a',v)
call assert_equal(3,h%length())
w => h%get('a')
call assert_equal(v,w)
! add map to map
call h%add('map',g)
call assert_equal(4,h%length())
