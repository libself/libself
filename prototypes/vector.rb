# Author: ralf.mueller

class Pair
  def initialize(key,value)
    @key = key
    @value = value
  end
  def has_key?(key)
    return @key == key
  end
end
class MyVector < Array
  def initialize(args=nil)
    if args.nil?
      super()
    else
      args.each {|i| self << i}
    end
  end
  def add(value)
    self << value unless self.include?(value)
    self
  end
  def push(value)
    self << value
    self
  end

  # a recursive key search
  def has_key?(key,recursive: false)
    if recursive then
      if self[0].kind_of? self.class
        #self.each {|i| pp [i.class,i.respond_to?(:has_key?),i.has_key?(key,recursive: true),self[2][0]]}
        ret = self.collect {|item| item.respond_to?(:has_key?) ? item.has_key?(key,recursive: true) : false}
#       pp ret
        ret = ret.flatten.inject {|sum,item| sum ||= item}
        return ret
      else
        return self[0] == key
      end
    else
#     pp [self[0],key]
      return self[0] == key
    end
  end
  def has_recKey?(key)
    has_key?(key, recursive: true)
  end
end
class MyHash < Hash
  def add(value)
    self[value] = {} unless self.keys.include?(value)
    self
  end
end
class MyKeyStore
  attr_accessor :values, :keys

  def initialize(keys: [],values: [])
    @keys   = Array.new(keys)
    @values = Array.new(values)
  end
  def set(key,value)
    keyIndex = @keys.index(key)
    if keyIndex.nil? then
      @keys.push(key)
      @values.push(value)
    else
      @values[keyIndex] = value
    end
  end
  def get(key)
    keyIndex = @keys.index(key)
    if keyIndex.nil? then
      warn "Could not find key '#{key}'"
    else
      @values[keyIndex]
    end
  end
  def empty?
    return @keys.empty?
  end
  def has_key?(key)
    return @keys.include?(key)
  end

  alias :add :set
  alias :items :values
  alias :[] :get
end
