# Author: ralf.mueller

$:.unshift File.join(File.dirname(__FILE__),".")
require 'vector'
require 'minitest/autorun'
require 'pp'

OPERATORS = %w[mean min max div]
INTERVALS = %w[PT06H P01D P01M P01Y P01D]
VARNAMES  = %w[u v temp zmc tend_t_up]

class MyListTest < Minitest::Test
  def setup
    @input = []
    OPERATORS.each {|operator| INTERVALS.each {|interval| VARNAMES.each {|varname|
          next if varname.size == 1 and interval == 'P01D'
          next if varname.size >  5 and interval == 'P01Y'
          next if varname.size == 4 and ['min','max'].include?(operator)
          @input << [operator,interval,varname]
    } } }

    @expectedHashOutput = {
      "PT06H"=> {
        "mean"=>["temp", "tend_t_up", "u", "v", "zmc"],
        "min"=>["tend_t_up", "u", "v", "zmc"],
        "max"=>["tend_t_up", "u", "v", "zmc"],
        "div"=>["temp", "tend_t_up", "u", "v", "zmc"]},
      "P01D"=> {
        "mean"=>["temp", "tend_t_up", "zmc"],
        "min"=>["tend_t_up", "zmc"],
        "max"=>["tend_t_up", "zmc"],
        "div"=>["temp", "tend_t_up", "zmc"]},
      "P01M"=> {
        "mean"=>["temp", "tend_t_up", "u", "v", "zmc"],
        "min"=>["tend_t_up", "u", "v", "zmc"],
        "max"=>["tend_t_up", "u", "v", "zmc"],
        "div"=>["temp", "tend_t_up", "u", "v", "zmc"]},
      "P01Y"=> {
        "mean"=>["temp", "u", "v", "zmc"],
        "min"=>["u", "v", "zmc"],
        "max"=>["u", "v", "zmc"],
        "div"=>["temp", "u", "v", "zmc"]}}


    @expectedVectorOutput = MyVector.new([
      MyVector.new(["PT06H",
        MyVector.new(["mean", MyVector.new(["temp", "tend_t_up", "u", "v", "zmc"])]),
        MyVector.new(["min" , MyVector.new(["tend_t_up", "u", "v", "zmc"])]),
        MyVector.new(["max" , MyVector.new(["tend_t_up", "u", "v", "zmc"])]),
        MyVector.new(["div" , MyVector.new(["temp", "tend_t_up", "u", "v", "zmc"])])]),
      MyVector.new(["PT01D",
        MyVector.new(["mean", MyVector.new(["temp", "tend_t_up", "zmc"])]),
        MyVector.new(["min" , MyVector.new(["tend_t_up", "zmc"])]),
        MyVector.new(["max" , MyVector.new(["tend_t_up", "zmc"])]),
        MyVector.new(["div" , MyVector.new(["temp", "tend_t_up", "zmc"])])]),
      MyVector.new(["P06M",
        MyVector.new(["mean", MyVector.new(["temp", "tend_t_up", "zmc"])]),
        MyVector.new(["min" , MyVector.new(["tend_t_up", "u", "v", "zmc"])]),
        MyVector.new(["max" , MyVector.new(["tend_t_up", "u", "v", "zmc"])]),
        MyVector.new(["div" , MyVector.new(["temp", "tend_t_up", "u", "v", "zmc"])])]),
      MyVector.new(["P06Y",
        MyVector.new(["mean", MyVector.new(["temp", "u", "v", "zmc"])]),
        MyVector.new(["min" , MyVector.new(["u", "v", "zmc"])]),
        MyVector.new(["max" , MyVector.new(["u", "v", "zmc"])]),
        MyVector.new(["div" , MyVector.new(["temp", "u", "v", "zmc"])])])])

    @expectedKeyStoreOutput = MyKeyStore.new
    @expectedKeyStoreOutput.add(
      "PT06H",
        MyKeyStore.new(keys:   ["mean","min","max","div"],
                       values: [MyVector.new(["temp", "tend_t_up", "u", "v", "zmc"]), MyVector.new(["tend_t_up", "u", "v", "zmc"]), MyVector.new(["tend_t_up", "u", "v", "zmc"]),MyVector.new(["temp", "tend_t_up", "u", "v", "zmc"])])
    )
    @expectedKeyStoreOutput.add(
      "PT01D",
      MyKeyStore.new(keys: ["mean","min","max","div"],
                     values: [ MyVector.new(["temp", "tend_t_up", "zmc"]), MyVector.new(["tend_t_up", "zmc"]), MyVector.new(["tend_t_up", "zmc"]), MyVector.new(["temp", "tend_t_up", "zmc"])])
    )
    @expectedKeyStoreOutput.add(
      "P06M",
      MyKeyStore.new(keys: ["mean","min","max","div"],
                     values: [ MyVector.new(["temp", "tend_t_up", "zmc"]), MyVector.new(["tend_t_up", "u", "v", "zmc"]), MyVector.new(["tend_t_up", "u", "v", "zmc"]), MyVector.new(["temp", "tend_t_up", "u", "v", "zmc"])]
                    )
    )
    @expectedKeyStoreOutput.add(
      "P06Y",
      MyKeyStore.new(keys: ["mean","min","max","div"],
                     values: [ MyVector.new(["temp", "u", "v", "zmc"]), MyVector.new(["u", "v", "zmc"]), MyVector.new(["u", "v", "zmc"]), MyVector.new(["temp", "u", "v", "zmc"])]
                    )
    )
  end

  def intervalHasEvent?(interval)
    return interval == (ENV.has_key?('INT') ? ENV['INT'] : 'P01D')
  end

  # multiple adds of the same value, should lead to a singe entry only
  def check(listObj)
    listObj.clear
    listObj.add('a').add('a').add('a')
    assert_equal(1,listObj.size)
    listObj.add('b').add('b')
    assert_equal(2,listObj.size)
    pp listObj
  end

  def test_add_multiple
    check(MyHash.new)
    check(MyVector.new)
  end

  # Hash and Array based
  # OUTPUT = {
  # "intervalA" => [
  #    { "operatorA" => [vA,vB,...]},
  #    { "operatorB" => [vA,vC,...]},
  #    ],
  # "intervalB" => [
  #    { "operatorA" => [vA,vB,...]},
  #    { "operatorB" => [vD,vC,...]},
  #    ],
  # }
  def loop_hash(output)
    output.each {|interval,joblist|
      if intervalHasEvent?(interval) then
        puts "INTERVAL:#{interval} is active"
        joblist.each {|operator,varlist|
          varlist.each {|var|
            puts "Work on VARNAME:#{var} for OPERATOR:#{operator}"
          }
        }
      end
    }
  end

  def test_hash_output
    output = {}
    @input.each {|line|
      operator,interval,varname = line
      ((output[interval] ||= {})[operator] ||= []) << varname
    }
    output.each {|interval,joblist| joblist.each {|operator,varlist| output[interval][operator] = varlist.sort.uniq}}
    loop_hash(output)
    assert_equal(@expectedHashOutput,output)
  end

  # MyVector based
  def loop_vector(output)
    puts "OUTPUT SIZE:#{output.size}"
    (0...output.size).each {|intervalIndex|
      intervalVector = output[intervalIndex]
      interval       = intervalVector[0]
      if intervalHasEvent?(interval) then
      else
        pp interval
      end
    }
  end

  def test_vector_output
#   pp @input
#   pp $expectedOutput[0].has_key?('intervalA')
#   return
    #pp $expectedOutput
    output         = MyVector.new
    intervalVector = MyVector.new
    operatorVector = MyVector.new
    varnameVector  = MyVector.new
    @input.each {|line|
      operator,interval,varname = line
      if output.has_recKey?(interval) then
        # select corresp. intervalVector
        intervalVector = output.select {|vector| vector[0] == interval}.first
      else
        # add empty intervalVector
        intervalVector = MyVector.new
        output.add(MyVector.new([interval,intervalVector]))
      end
        intervalVector = output.select {|vector| vector[0] == interval}.first
        intervalVector.add(MyVector.new([operator,varname]))
    }
    pp output
    loop_vector(output)
  end

  def test_vector_new
    v = MyVector.new([1,2,3])
    w = MyVector.new
    w.add(1)
    w.add(2)
    w.add(3)
    assert_equal(v,w)

    v = MyVector.new([1,MyVector.new([2]),MyVector.new([3])])
    assert_equal([1,[2],[3]],v)
  end

  def test_vector_hasKey
    v = MyVector.new(["key","value"])
    assert_equal(true,v.has_recKey?('key'))

    w = MyVector.new([:first,v])
    assert_equal(true,w.has_recKey?(:first))
    assert_equal(w.has_key?(:first),w.has_recKey?(:first))

    w_reverse = MyVector.new([v,:first])
    assert_equal(true,w_reverse.has_recKey?("key"))
    assert_equal(true,w_reverse.has_key?(v))

    msg = lambda {|name| "Could not find key '#{name}'"}
    x = MyVector.new([
                     MyVector.new(['last','first']),
                     MyVector.new([v,w]),
                     MyVector.new([-1,:last])
    ])
    assert_equal(true , x.has_recKey?('last')      , msg['last'])
    assert_equal(true , x.has_recKey?(:first)      , msg[':first'])
    assert_equal(false, x.has_key?(:first)         , msg[':first'])
    assert_equal(true , x.has_recKey?(-1)          , msg[-1])

    assert_equal(false, x.has_recKey?(v)           , msg[v.to_s])
    assert_equal(false, x.has_key?(v)              , msg[v.to_s])
    assert_equal(true , x.has_recKey?('key')       , msg['key'])

    assert_equal(false, x.has_recKey?('sdfsdfwsfd'), msg['sdfsdfwsfd'])
    assert_equal(false, x.has_key?('sdfsdfwsfd')   , msg['sdfsdfwsfd'])
  end

  def test_vector_of_pairs
    p = Pair.new('key','value')
    v = MyVector.new
    v.add(p)
    v.add(p)
    pp v
    assert_equal(true,p.has_key?('key'))
    assert_equal(false,v.has_key?('key'))
   # assert_equal(true,v.has_recKey?('key'))
  end

  def test_expectedVectorOutput
    pp @expectedVectorOutput
    pp @expectedOutput
    assert_equal(true,@expectedOutput.has_key?('intervalA',recursive: true))
    assert_equal(true,@expectedVectorOutput.has_key?('PT01D',recursive: true))
    assert_equal(false,@expectedVectorOutput.has_key?('mean',recursive: true))
  end
  def test_keyStore
    ks = MyKeyStore.new
    assert(ks.empty?,"KeyStrore not empty")

    meanVars = MyVector.new(["temp", "tend_t_up", "u", "v", "zmc"])
    maxVars  = MyVector.new(["temp", "tend_t_up", "zmc"])
    meanks   = MyKeyStore.new
    meanks.add('mean',meanVars)
    meanks.add('max',maxVars)
    assert_equal(meanVars,meanks.get('mean'))
    assert(meanVars != meanks.get('max'))
    assert_equal(meanks.get('max'),meanks['max'])

    assert(meanks.has_key?('mean'))
    assert(meanks.has_key?('max'))
    assert(!meanks.has_key?('min'))
  end
  def test_KeyStoreOutput
    pp @expectedKeyStoreOutput
  end
end
